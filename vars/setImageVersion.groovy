def call(String majorVersion) {
    def newVersion = "${majorVersion}.${env.BUILD_NUMBER}-${getCommitId()}"
    echo "New version set to: ${newVersion}"
    return newVersion
}