def call (Map vars) {
    // call funtion example: robotTestFramework(
    //                              robotFileLocation: 'The location of the robot file from project dir',
    //                              robotTag: 'Tag to run robot'
    // )
    sh "robot ${vars.robotFileLocation} ${vars.robotTag}"
    step(
            [
                    $class: 'RobotPublisher',
                    outputPath: '.',
                    passThreshold: 100,
                    unstableThreshold: 100,
                    otherFiles: '*.png'
            ]
    )
}
